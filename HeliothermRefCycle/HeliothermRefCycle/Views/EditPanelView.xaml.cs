﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HeliothermRefCycle.ViewModels;
using PropertyChanged;

namespace HeliothermRefCycle.Views
{
    /// <summary>
    /// Interaktionslogik für EditPanelView.xaml
    /// </summary>
    [AddINotifyPropertyChangedInterface]
    public partial class EditPanelView : UserControl
    {
        private BasePanelViewModel curViewModel;

        private void UpdateViewModel(object sender, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            if (DataContext is CoolingCircuitPanelViewModel)
            {
                curViewModel = DataContext as CoolingCircuitPanelViewModel;
                var view = new CoolingCircuitPanelEditView();
                view.DataContext = curViewModel;
                this.Content = view;
            }
            else if (DataContext is GraphPanelViewModel)
            {
                curViewModel = DataContext as GraphPanelViewModel;
                var view = new GraphPanelEditView();
                view.DataContext = curViewModel;
                this.Content = view;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public EditPanelView()
        {
            InitializeComponent();
            this.DataContextChanged += UpdateViewModel;
        }
    }
}
