﻿
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows.Navigation;
using System.Xml;
using PropertyChanged;

namespace HeliothermRefCycle.Models 
{
    public class Point
    {
        public int X { get; set; }
        public string Y { get; set; }
    }


    [AddINotifyPropertyChangedInterface]
    public class Sensor
    {
        public int Index { get; set; } = 0;

        public int MaxSize { get; set; }

        public string Name { get; set; }

        public int XResolution { get; set; }

        public void SetValue(string val) 
        {
            CurrentVal = val;
            ValueList.Add(new Point() { X = Index++*XResolution, Y = val });

            if (ValueList.Count > MaxSize) 
            {
                ValueList.RemoveAt(0);
            }
        }

        public string CurrentVal { get; private set; }


        public Sensor(string name)
        {
            Name = name;
        }

        public ObservableCollection<Point> ValueList { get; private set; } = new ObservableCollection<Point>();
    }

    [AddINotifyPropertyChangedInterface]
    public class RefrigerationCycle 
    {
        public ObservableConcurrentDictionary<int, Sensor> Sensors { get; set; } = new ObservableConcurrentDictionary<int, Sensor>();

        /// <summary>
        /// Create a new Sensor key-value pair
        /// </summary>
        /// <param name="name"></param>
        public void CreateNewSensor(int key,string name)
        {
            Sensors.Add(key, new Sensor(name));
        }

        /// <summary>
        /// Add a new value to the internal list for a specific Sensor
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddNewValue(int key, string value)
        {

            Sensors[key].SetValue(value);
        }

        public void SaveAsXMl(string filename)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode dataPoints = doc.CreateElement("DataPoints");
            doc.AppendChild(dataPoints);


            foreach (var s in Sensors)
            {
                XmlNode dataPoint = doc.CreateElement("DataPoint");
                XmlAttribute attName = doc.CreateAttribute("id");
                attName.Value = s.Key.ToString();
                dataPoint.Attributes.Append(attName);

                for (int i = 0; i < s.Value.ValueList.Count; i++)
                {
                    XmlNode measurement = doc.CreateElement("Measurement");
                    XmlAttribute attVal = doc.CreateAttribute("value");
                    attVal.Value = s.Value.ValueList[i].Y;
                    measurement.Attributes.Append(attVal);
                    dataPoint.AppendChild(measurement);
                }

                dataPoints.AppendChild(dataPoint);
            }
          
           
            doc.Save(filename);
        }

        public Dictionary<int, Sensor> LoadFromXMl(string filename)
        {

            Dictionary<int, Sensor> tmp = new Dictionary<int, Sensor>();


            XmlDocument doc = new XmlDocument();
            doc.Load(filename);

            XmlNodeList elemList = doc.GetElementsByTagName("DataPoint");

            foreach (XmlNode e in elemList)
            {

                string name = e.Attributes["id"]?.InnerText;
                tmp.Add(Convert.ToInt32(name),new Sensor("Testname"));
                foreach (XmlNode c in e.ChildNodes)
                {
                    string value = c.Attributes["value"]?.InnerText;
                    tmp[Convert.ToInt32(name)].SetValue(value);
                }
            }

            return tmp;
        }

       
    }
}
