﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HeliothermRefCycle.Models
{

    public struct JsonValues
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("value")]
        public float Value { get; set; }
    }

    public struct JsonResponse
    {
        [JsonProperty("result")]
        public bool Result { get; set; }

        public JsonValues[] Values { get; set; }
    }


}
