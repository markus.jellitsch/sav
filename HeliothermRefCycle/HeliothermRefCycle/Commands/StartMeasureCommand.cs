﻿using System;
using System.Windows.Input;
using HeliothermRefCycle.ViewModels;

namespace HeliothermRefCycle.Models
{
    internal class StartMeasureCommand : ICommand
    {
  
        public MainViewModel viewmodel;

        public StartMeasureCommand(MainViewModel model)
        {
            viewmodel = model;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event System.EventHandler CanExecuteChanged
        { 
        
            add { CommandManager.RequerySuggested += value;}
            remove { CommandManager.RequerySuggested += value; }
        }

        public void Execute(object parameter)
        {
            if (viewmodel != null)
            {
                viewmodel.StartMeasurement();
            }
        }
    }
}