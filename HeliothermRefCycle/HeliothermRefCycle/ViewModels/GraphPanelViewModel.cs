﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using HeliothermRefCycle.Models;
using HeliothermRefCycle.Views;
using Newtonsoft.Json;
using PropertyChanged;

namespace HeliothermRefCycle.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class SensorWithNameAndColor
    {
        private string name;
        public event EventHandler NameChanged;

        public string Name
        {
            get => name;
            set
            {
                name = value;
                OnNameChanged();
            }
        }

        public Color ColorProp => Color.FromRgb(ColorR, ColorG, ColorB);
        
        [AlsoNotifyFor(nameof(ColorProp))]
        public byte ColorR { get; set; }
        [AlsoNotifyFor(nameof(ColorProp))]
        public byte ColorG { get; set; }
        [AlsoNotifyFor(nameof(ColorProp))]
        public byte ColorB { get; set; }

        public string DisplayName { get; set; }
        
        [JsonIgnore]
        public Sensor SensorProp { get; set; }

        protected virtual void OnNameChanged()
        {
            NameChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    [AddINotifyPropertyChangedInterface]
    public sealed class GraphPanelViewModel : BasePanelViewModel
    {
        private RefrigerationCycle refcycle;

        public override BasePanelViewModel Copy()
        {
            var viewModel = this.MemberwiseClone() as GraphPanelViewModel;

            viewModel.Sensors = new ObservableCollection<SensorWithNameAndColor>();
            foreach (var sensor in Sensors)
            {
                viewModel.Sensors.Add(new SensorWithNameAndColor(){DisplayName = sensor.DisplayName, Name = sensor.Name,
                    SensorProp = sensor.SensorProp, ColorR = sensor.ColorR, ColorG = sensor.ColorG, ColorB = sensor.ColorB});
            }
            foreach (var sensor in viewModel.Sensors)
            {
                sensor.NameChanged += viewModel.UpdateSensors;
            }

            return viewModel;
        }

        [JsonIgnore]
        public override RefrigerationCycle Refcycle
        {
            get => refcycle;
            set
            {
                refcycle = value;
                UpdateSensors(this, EventArgs.Empty);
            }
        }

        public override double Width { get; set; }
        public override double Height { get; set; }

        public override double PosX { get; set; } = 0;
        public override double PosY { get; set; } = 0;

        public string AxisXName { get; set; } = "";
        public string AxisYName { get; set; } = "";

        public bool ShowLegend { get; set; } = true;
        public string Title { get; set; } = "";

        public ObservableCollection<SensorWithNameAndColor> Sensors { get; set; } = new ObservableCollection<SensorWithNameAndColor>()
        {
            {new SensorWithNameAndColor(){Name="EQ Temperature (In)", DisplayName = "Sensor 1:", SensorProp = null, ColorR = 0, ColorB = 0, ColorG = 0}},
            {new SensorWithNameAndColor(){Name="EQ Temperature (Out)", DisplayName = "Sensor 2:", SensorProp = null, ColorR = 255, ColorB = 0, ColorG = 0}},
            {new SensorWithNameAndColor(){Name="10", DisplayName = "Sensor 3:", SensorProp = null, ColorR = 0, ColorB = 255, ColorG = 0}},
            {new SensorWithNameAndColor(){Name="", DisplayName = "Sensor 4:", SensorProp = null, ColorR = 0, ColorB = 0, ColorG = 255}},
        };

        [JsonConstructor]
        private GraphPanelViewModel()
        {
            Sensors.Clear();
        }

        public GraphPanelViewModel(RefrigerationCycle rc)
        {
            Refcycle = rc;

            var view = new CoolingCircuitPanelView {DataContext = this};

            foreach (var sensor in Sensors)
            {
                sensor.NameChanged += UpdateSensors;
            }
        }
        
        /// <summary>
        /// Check all sensor names and update the Sensor.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateSensors(object sender, EventArgs e)
        {
            foreach (var sensor in Sensors)
            {
                int key;

                if (Int32.TryParse(sensor.Name, out key) && Refcycle.Sensors.ContainsKey(key))
                {
                    sensor.SensorProp = Refcycle.Sensors[key];
                }
                else
                {
                    sensor.SensorProp = Refcycle.Sensors.FirstOrDefault(s => s.Value.Name == sensor.Name).Value;
                }
            }
        }
    }
}
