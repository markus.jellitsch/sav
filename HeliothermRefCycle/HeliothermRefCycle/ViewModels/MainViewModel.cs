﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web.UI.WebControls;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using HeliothermRefCycle.Models;
using HeliothermRefCycle.Views;
using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PropertyChanged;
using RestSharp;

namespace HeliothermRefCycle.ViewModels 
{
    enum EDirection
    {
        Top,
        Bottom,
        Left,
        Right
    }

    [AddINotifyPropertyChangedInterface]
    class ResizeBar
    {
        public double PosX { get; set; }
        public double PosY { get; set; }
        public double Width { get; set; } = 1;
        public double Height { get; set; } = 1;

        public ResizeBar() { }

        public ResizeBar(ResizeBar rb)
        {
            PosX = rb.PosX;
            PosY = rb.PosY;
            Width = rb.Width;
            Height = rb.Height;
        }
    }

    struct SettingsToSave
    {
        public int UpdateIntervall;

        public int MaxValueListSize;

        public string ServerIp;
        public int ServerPort;

        public ObservableCollection<BasePanelViewModel> ViewModels;
    }

    public class RelayCommand : ICommand
    {
        readonly Action<object> execute;
        readonly Predicate<object> canExecute;

        public RelayCommand(Action<object> execute) : this(execute, null)
        {
        }

        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
            {
                throw new ArgumentNullException(nameof(execute));
            }

            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return canExecute == null || canExecute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        public void Execute(object parameter)
        {
            execute(parameter);
        }
    }

    [AddINotifyPropertyChangedInterface]
    class MainViewModel
    {
        private readonly BackgroundWorker worker;
        private const string DefaultFileName = "./DefaultLayout.json";
        private const int MinPanelSize = 20;
        private const double DoubleTolerance = 0.001;
        private const int ResizeTolerance = 3;
        private const double ResizeSnapDistance = 12;
        private const int MinUpdateIntervall = 1;
        private const int MaxUpdateIntervall = 60;
        private const int MinMaxValueListSize = 1;
        private const int MaxMaxValueListSize = 1000;
        private const EDirection DefaultAddDirection = EDirection.Bottom;

        private int updateIntervall;
        private int maxValueListSize;

        private string serverIP = "";
        private int serverPort = 8000;

        #region Properties
        public RefrigerationCycle Refcycle { get; set; } = new RefrigerationCycle();

        public ObservableCollection<BasePanelViewModel> ViewModels { get; set; } = new ObservableCollection<BasePanelViewModel>();
        public ObservableCollection<ResizeBar> HorizontalResizeBars { get; set; } = new ObservableCollection<ResizeBar>();
        public ObservableCollection<ResizeBar> VerticalResizeBars { get; set; } = new ObservableCollection<ResizeBar>();

        public double PanelWidth { get; set; }
        public double PanelHeight { get; set; }

        public BasePanelViewModel CurrentlyRightClickedViewModel { get; set; }
        public bool CanRemoveCurrentlyRightClickedViewModel { get; set; }

        [DoNotNotify]
        public BasePanelViewModel DefaultViewModel => new CoolingCircuitPanelViewModel(Refcycle);

        public ResizeBar HoveredResizeBar { get; set; }
        public bool CurrentlyResizing { get; set; }
        public double ResizeStartPos { get; set; }
        public double ResizeMaxPos { get; set; }
        public double ResizeMinPos { get; set; }

        public double PanelMouseX { get; set; }
        public double PanelMouseY { get; set; }

        public bool DoubleClicked { get; set; } = false;

        public BasePanelViewModel CurrentlyDraggedViewModel { get; set; } = null;
        public EDirection CurrentAddDirection { get; set; } = DefaultAddDirection;

        public bool Connected { get; set; } = false;

        public int UpdateIntervall
        {
            get { return updateIntervall; }
            set
            {
                value = Math.Max(Math.Min(value, MaxUpdateIntervall), MinUpdateIntervall);
                updateIntervall = value;
                foreach (var sensor in Refcycle.Sensors)
                {
                    sensor.Value.XResolution = value;
                    sensor.Value.ValueList.Clear();
                    sensor.Value.Index = 0;
                }
            }
        }

        public int MaxValueListSize
        {
            get { return maxValueListSize; }
            set
            {
                value = Math.Max(Math.Min(value, MaxMaxValueListSize), MinMaxValueListSize);
                maxValueListSize = value;
                foreach (var sensor in Refcycle.Sensors)
                {
                    sensor.Value.MaxSize = value;
                    sensor.Value.ValueList.Clear();
                    sensor.Value.Index = 0;
                }
            }
        }

        public string ServerIp 
        {
            get { return serverIP; }
            set 
            {
                serverIP = value;
                foreach (var sensor in Refcycle.Sensors)
                {
                    sensor.Value.ValueList.Clear();
                    sensor.Value.Index = 0;
                }

            }
        
        }

        public int ServerPort
        {
            get { return serverPort; }
            set
            {
                serverPort = value;
                foreach (var sensor in Refcycle.Sensors)
                {
                    sensor.Value.ValueList.Clear();
                    sensor.Value.Index = 0;
                }
            }
        }

        #endregion

        #region Commands
        private ICommand editCommand;
        public ICommand EditPressed
        {
            get
            {
                if (editCommand == null)
                {
                    editCommand = new RelayCommand(EditPressedExecute, param => CurrentlyRightClickedViewModel != null);
                }
                return editCommand;
            }
        }

        /// <summary>
        /// Open the edit window.
        /// </summary>
        /// <param name="param"></param>
        private void EditPressedExecute(object param)
        {
            OpenEditWindow(CurrentlyRightClickedViewModel);
        }

        private ICommand addCommand;
        public ICommand AddPressed
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new RelayCommand(AddPressedExecute, param => true);
                }
                return addCommand;
            }
        }

        /// <summary>
        /// Adds the default viewModel.
        /// </summary>
        /// <param name="param"></param>
        private void AddPressedExecute(object param)
        {
            AddViewModel(DefaultViewModel);
        }

        private ICommand removeCommand;
        public ICommand RemovePressed
        {
            get
            {
                if (removeCommand == null)
                {
                    removeCommand = new RelayCommand(RemovePressedExecute, param => CanRemoveCurrentlyRightClickedViewModel);
                }
                return removeCommand;
            }
        }

        /// <summary>
        /// Removes the right clicked viewModel.
        /// </summary>
        /// <param name="param"></param>
        private void RemovePressedExecute(object param)
        {
            RemoveViewModel(CurrentlyRightClickedViewModel);
        }

        private ICommand contextMenuOpenedCommand;
        public ICommand ContextMenuOpened
        {
            get
            {
                if (contextMenuOpenedCommand == null)
                {
                    contextMenuOpenedCommand = new RelayCommand(ContextMenuOpenedExecute, param => true);
                }
                return contextMenuOpenedCommand;
            }
        }

        /// <summary>
        /// Identifies the right clicked viewModel and checks if it can be removed.
        /// </summary>
        /// <param name="param"></param>
        private void ContextMenuOpenedExecute(object param)
        {
            CurrentlyRightClickedViewModel = null;
            CanRemoveCurrentlyRightClickedViewModel = false;

            foreach (var curViewModel in ViewModels)
            {
                if (PanelMouseX >= curViewModel.PosX && PanelMouseY >= curViewModel.PosY &&
                    PanelMouseX < curViewModel.PosX + curViewModel.Width &&
                    PanelMouseY < curViewModel.PosY + curViewModel.Height)
                {
                    CurrentlyRightClickedViewModel = curViewModel;
                    break;
                }
            }

            if (ViewModels.Count == 1 && CurrentlyRightClickedViewModel != null)
            {
                CanRemoveCurrentlyRightClickedViewModel = true;
            }
            else if (CurrentlyRightClickedViewModel != null)
            {
                foreach (EDirection direction in Enum.GetValues(typeof(EDirection)))
                {
                    BasePanelViewModel viewModelMin = null;
                    BasePanelViewModel viewModelMax = null;

                    if (direction == EDirection.Top)
                    {
                        foreach (var curViewModel in ViewModels)
                        {
                            if (curViewModel != CurrentlyRightClickedViewModel)
                            {
                                if (Math.Abs(curViewModel.PosY + curViewModel.Height - CurrentlyRightClickedViewModel.PosY) <
                                    DoubleTolerance)
                                {
                                    if (Math.Abs(curViewModel.PosX - CurrentlyRightClickedViewModel.PosX) < DoubleTolerance &&
                                        curViewModel.Width - CurrentlyRightClickedViewModel.Width < DoubleTolerance)
                                    {
                                        viewModelMin = curViewModel;
                                    }
                                    if (Math.Abs(curViewModel.PosX + curViewModel.Width -
                                                      (CurrentlyRightClickedViewModel.PosX + CurrentlyRightClickedViewModel.Width)) < DoubleTolerance &&
                                             curViewModel.Width - CurrentlyRightClickedViewModel.Width < DoubleTolerance)
                                    {
                                        viewModelMax = curViewModel;
                                    }
                                }
                            }
                        }
                    }
                    else if (direction == EDirection.Bottom)
                    {
                        foreach (var curViewModel in ViewModels)
                        {
                            if (curViewModel != CurrentlyRightClickedViewModel)
                            {
                                if (Math.Abs(curViewModel.PosY - (CurrentlyRightClickedViewModel.PosY + CurrentlyRightClickedViewModel.Height)) <
                                    DoubleTolerance)
                                {
                                    if (Math.Abs(curViewModel.PosX - CurrentlyRightClickedViewModel.PosX) < DoubleTolerance &&
                                        curViewModel.Width - CurrentlyRightClickedViewModel.Width < DoubleTolerance)
                                    {
                                        viewModelMin = curViewModel;
                                    }
                                    if (Math.Abs(curViewModel.PosX + curViewModel.Width -
                                                      (CurrentlyRightClickedViewModel.PosX + CurrentlyRightClickedViewModel.Width)) < DoubleTolerance &&
                                             curViewModel.Width - CurrentlyRightClickedViewModel.Width < DoubleTolerance)
                                    {
                                        viewModelMax = curViewModel;
                                    }
                                }
                            }
                        }
                    }
                    else if (direction == EDirection.Left)
                    {
                        foreach (var curViewModel in ViewModels)
                        {
                            if (curViewModel != CurrentlyRightClickedViewModel)
                            {
                                if (Math.Abs(curViewModel.PosX + curViewModel.Width - CurrentlyRightClickedViewModel.PosX) <
                                    DoubleTolerance)
                                {
                                    if (Math.Abs(curViewModel.PosY - CurrentlyRightClickedViewModel.PosY) < DoubleTolerance &&
                                        curViewModel.Height - CurrentlyRightClickedViewModel.Height < DoubleTolerance)
                                    {
                                        viewModelMin = curViewModel;
                                    }
                                    if (Math.Abs(curViewModel.PosY + curViewModel.Height -
                                                      (CurrentlyRightClickedViewModel.PosY + CurrentlyRightClickedViewModel.Height)) < DoubleTolerance &&
                                             curViewModel.Height - CurrentlyRightClickedViewModel.Height < DoubleTolerance)
                                    {
                                        viewModelMax = curViewModel;
                                    }
                                }
                            }
                        }
                    }
                    else if (direction == EDirection.Right)
                    {
                        foreach (var curViewModel in ViewModels)
                        {
                            if (curViewModel != CurrentlyRightClickedViewModel)
                            {
                                if (Math.Abs(curViewModel.PosX - (CurrentlyRightClickedViewModel.PosX + CurrentlyRightClickedViewModel.Width)) <
                                    DoubleTolerance)
                                {
                                    if (Math.Abs(curViewModel.PosY - CurrentlyRightClickedViewModel.PosY) < DoubleTolerance &&
                                        curViewModel.Height - CurrentlyRightClickedViewModel.Height < DoubleTolerance)
                                    {
                                        viewModelMin = curViewModel;
                                    }
                                    if (Math.Abs(curViewModel.PosY + curViewModel.Height -
                                                      (CurrentlyRightClickedViewModel.PosY + CurrentlyRightClickedViewModel.Height)) < DoubleTolerance &&
                                             curViewModel.Height - CurrentlyRightClickedViewModel.Height < DoubleTolerance)
                                    {
                                        viewModelMax = curViewModel;
                                    }
                                }
                            }
                        }
                    }

                    if (viewModelMin != null && viewModelMax != null)
                    {
                        CanRemoveCurrentlyRightClickedViewModel = true;
                        break;
                    }
                }
            }
        }

        private ICommand gridResizeCommand;
        public ICommand GridResized
        {
            get
            {
                if (gridResizeCommand == null)
                {
                    gridResizeCommand = new RelayCommand(GridResizedExecute, param => true);
                }
                return gridResizeCommand;
            }
        }

        /// <summary>
        /// Resizes all viewModels and updates all resizeBars.
        /// </summary>
        /// <param name="param"></param>
        private void GridResizedExecute(object param)
        {
            var element = param as FrameworkElement;
            if (element == null)
                return;

            if (Math.Abs(PanelWidth) < DoubleTolerance || Math.Abs(PanelHeight) < DoubleTolerance)
            {
                PanelWidth = element.ActualWidth;
                PanelHeight = element.ActualHeight;
                return;
            }

            var newWidth = element.ActualWidth;
            var newHeight = element.ActualHeight;

            var scaleX = 1.0 * newWidth / PanelWidth;
            var scaleY = 1.0 * newHeight / PanelHeight;

            foreach (var curViewModel in ViewModels)
            {
                double newW = ((curViewModel.PosX + curViewModel.Width) * scaleX);
                double newH = ((curViewModel.PosY + curViewModel.Height) * scaleY);
                curViewModel.PosX = (curViewModel.PosX * scaleX);
                curViewModel.PosY = (curViewModel.PosY * scaleY);
                curViewModel.Width = newW - curViewModel.PosX;
                curViewModel.Height = newH - curViewModel.PosY;
            }

            PanelWidth = newWidth;
            PanelHeight = newHeight;

            UpdateResizeBars();
        }

        private ICommand windowLoadCommand;
        public ICommand WindowLoad
        {
            get
            {
                if (windowLoadCommand == null)
                {
                    windowLoadCommand = new RelayCommand(WindowLoadExecute, param => true);
                }
                return windowLoadCommand;
            }
        }

        /// <summary>
        /// Tries to load a default layout from a file.
        /// </summary>
        /// <param name="param"></param>
        private void WindowLoadExecute(object param)
        {
            var element = param as FrameworkElement;
            if (element == null)
                return;
            
            PanelWidth = element.ActualWidth;
            PanelHeight = element.ActualHeight;

            try
            {
                string json = File.ReadAllText(DefaultFileName);
                var settings = JsonConvert.DeserializeObject<SettingsToSave>(json, new JsonSerializerSettings{TypeNameHandling = TypeNameHandling.Auto});
                ViewModels = settings.ViewModels;
                UpdateIntervall = settings.UpdateIntervall;
                MaxValueListSize = settings.MaxValueListSize;
                ServerIp = settings.ServerIp;
                ServerPort = settings.ServerPort;
            }
            catch (Exception e)
            {
                return;
            }

            foreach (var viewModel in ViewModels)
            {
                viewModel.Refcycle = Refcycle;
            }
                
            double oldWidth = 0;
            double oldHeight = 0;

            foreach (var curViewModel in ViewModels)
            {
                oldWidth = Math.Max(oldWidth, curViewModel.PosX + curViewModel.Width);
                oldHeight = Math.Max(oldHeight, curViewModel.PosY + curViewModel.Height);
            }

            var scaleX = 1.0 * PanelWidth / oldWidth;
            var scaleY = 1.0 * PanelHeight / oldHeight;

            foreach (var curViewModel in ViewModels)
            {
                double newW = ((curViewModel.PosX + curViewModel.Width) * scaleX);
                double newH = ((curViewModel.PosY + curViewModel.Height) * scaleY);
                curViewModel.PosX = (curViewModel.PosX * scaleX);
                curViewModel.PosY = (curViewModel.PosY * scaleY);
                curViewModel.Width = newW - curViewModel.PosX;
                curViewModel.Height = newH - curViewModel.PosY;
            }

            UpdateResizeBars();
        }

        private ICommand mouseMoveCommand;
        public ICommand MouseMove
        {
            get
            {
                if (mouseMoveCommand == null)
                {
                    mouseMoveCommand = new RelayCommand(MouseMoveExecute, param => true);
                }
                return mouseMoveCommand;
            }
        }

        /// <summary>
        /// Records the mouse position relative to the main ui element. Also changes the mouse icon when the mouse is near ResizeBars.
        /// It also move the currently dragged ResizeBar.
        /// </summary>
        /// <param name="param"></param>
        private void MouseMoveExecute(object param)
        {
            var mouseCoords = Mouse.GetPosition(param as IInputElement);

            PanelMouseX = mouseCoords.X;
            PanelMouseY = mouseCoords.Y;

            ResizeBar curResizeBar = null;

            if (CurrentlyResizing == false)
            {
                foreach (var resizeBar in HorizontalResizeBars)
                {
                    if (PanelMouseX >= resizeBar.PosX && PanelMouseX < resizeBar.PosX + resizeBar.Width)
                    {
                        if (PanelMouseY >= resizeBar.PosY - ResizeTolerance && PanelMouseY < resizeBar.PosY + resizeBar.Height + 2 * ResizeTolerance + 1)
                        {
                            Mouse.OverrideCursor = Cursors.SizeNS;
                            curResizeBar = resizeBar;
                            break;
                        }
                    }
                }

                if (curResizeBar == null)
                {
                    foreach (var resizeBar in VerticalResizeBars)
                    {
                        if (PanelMouseY >= resizeBar.PosY && PanelMouseY < resizeBar.PosY + resizeBar.Height)
                        {
                            if (PanelMouseX >= resizeBar.PosX - ResizeTolerance && PanelMouseX < resizeBar.PosX + resizeBar.Width + 2 * ResizeTolerance + 1)
                            {
                                Mouse.OverrideCursor = Cursors.SizeWE;
                                curResizeBar = resizeBar;
                                break;
                            }
                        }
                    }
                }

                if (curResizeBar == null)
                {
                    Mouse.OverrideCursor = null;
                    HoveredResizeBar = null;
                }
                else
                {
                    HoveredResizeBar = curResizeBar;
                }
            }
            else
            {
                if (HorizontalResizeBars.Contains(HoveredResizeBar))
                {
                    if(PanelMouseY < ResizeMinPos)
                        HoveredResizeBar.PosY = ResizeMinPos;
                    else if (PanelMouseY > ResizeMaxPos)
                        HoveredResizeBar.PosY = ResizeMaxPos;
                    else
                    {
                        HoveredResizeBar.PosY = PanelMouseY;

                        foreach (var rb in HorizontalResizeBars)
                        {
                            if (Math.Abs(rb.PosY - HoveredResizeBar.PosY) < ResizeSnapDistance && HoveredResizeBar != rb)
                            {
                                if (Math.Abs(rb.PosX - (HoveredResizeBar.PosX + HoveredResizeBar.Width)) < DoubleTolerance || Math.Abs(rb.PosX + rb.Width - HoveredResizeBar.PosX) < DoubleTolerance)
                                {
                                    HoveredResizeBar.PosY = rb.PosY;
                                    break;
                                }
                            }
                        }
                    }
                }
                else if (VerticalResizeBars.Contains(HoveredResizeBar))
                {
                    if (PanelMouseX < ResizeMinPos)
                        HoveredResizeBar.PosX = ResizeMinPos;
                    else if (PanelMouseX > ResizeMaxPos)
                        HoveredResizeBar.PosX = ResizeMaxPos;
                    else
                    {
                        HoveredResizeBar.PosX = PanelMouseX;

                        foreach (var rb in VerticalResizeBars)
                        {
                            if (Math.Abs(rb.PosX - HoveredResizeBar.PosX) < ResizeSnapDistance && HoveredResizeBar != rb)
                            {
                                if (Math.Abs(rb.PosY - (HoveredResizeBar.PosY + HoveredResizeBar.Height)) < DoubleTolerance || Math.Abs(rb.PosY + rb.Height - HoveredResizeBar.PosY) < DoubleTolerance)
                                {
                                    HoveredResizeBar.PosX = rb.PosX;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        private ICommand mouseLeftButtonDownCommand;
        public ICommand MouseLeftButtonDown
        {
            get
            {
                if (mouseLeftButtonDownCommand == null)
                {
                    mouseLeftButtonDownCommand = new RelayCommand(MouseLeftButtonDownExecute, param => true);
                }
                return mouseLeftButtonDownCommand;
            }
        }

        /// <summary>
        /// For single clicks it handeles the start of a drag or resize. For double clicks it openes the OpenEditWindow with the clicked viewModel
        /// </summary>
        /// <param name="param"></param>
        private void MouseLeftButtonDownExecute(object param)
        {
            if (param is MouseButtonEventArgs arg)
            {
                if(arg.ClickCount == 1)
                {
                    if (HoveredResizeBar != null)
                    {
                        if (HorizontalResizeBars.Contains(HoveredResizeBar))
                        {
                            CurrentlyResizing = true;
                            HorizontalResizeBars.Add(new ResizeBar(HoveredResizeBar));
                            ResizeStartPos = HoveredResizeBar.PosY;

                            double newWidth = HoveredResizeBar.Width;
                            double newPosX = HoveredResizeBar.PosX;

                            double curWidth;
                            double curPosX;

                            do
                            {
                                curWidth = newWidth;
                                curPosX = newPosX;

                                foreach (var rb in HorizontalResizeBars)
                                {
                                    if (Math.Abs(rb.PosY - HoveredResizeBar.PosY) < DoubleTolerance)
                                    {
                                        if (Math.Abs(rb.PosX - (curPosX + curWidth)) < DoubleTolerance)
                                        {
                                            newWidth += rb.Width;
                                        }
                                        else if (Math.Abs(rb.PosX + rb.Width - curPosX) < DoubleTolerance)
                                        {
                                            newWidth += rb.Width;
                                            newPosX = rb.PosX;
                                        }
                                    }
                                }
                            } while (Math.Abs(curWidth - newWidth) > DoubleTolerance && Math.Abs(curPosX - newPosX) > DoubleTolerance);

                            HoveredResizeBar.Width = newWidth;
                            HoveredResizeBar.PosX = newPosX;

                            ResizeMinPos = 0;
                            ResizeMaxPos = PanelHeight;

                            foreach (var curViewModel in ViewModels)
                            {
                                if ((curViewModel.PosX > HoveredResizeBar.PosX && curViewModel.PosX < HoveredResizeBar.PosX + HoveredResizeBar.Width) ||
                                    (curViewModel.PosX + curViewModel.Width > HoveredResizeBar.PosX && curViewModel.PosX + curViewModel.Width < HoveredResizeBar.PosX + HoveredResizeBar.Width) ||
                                    (curViewModel.PosX <= HoveredResizeBar.PosX && curViewModel.PosX + curViewModel.Width >= HoveredResizeBar.PosX + HoveredResizeBar.Width))
                                {
                                    if (curViewModel.PosY < HoveredResizeBar.PosY)
                                        ResizeMinPos = Math.Max(ResizeMinPos, curViewModel.PosY + MinPanelSize);

                                    if (curViewModel.PosY + curViewModel.Height > HoveredResizeBar.PosY)
                                        ResizeMaxPos = Math.Min(ResizeMaxPos, curViewModel.PosY + curViewModel.Height - MinPanelSize);
                                }
                            }

                            ResizeMinPos = Math.Min(ResizeMinPos, HoveredResizeBar.PosY);
                            ResizeMaxPos = Math.Max(ResizeMaxPos, HoveredResizeBar.PosY);
                        }
                        else if (VerticalResizeBars.Contains(HoveredResizeBar))
                        {
                            CurrentlyResizing = true;
                            VerticalResizeBars.Add(new ResizeBar(HoveredResizeBar));
                            ResizeStartPos = HoveredResizeBar.PosX;

                            double newHeight = HoveredResizeBar.Height;
                            double newPosY = HoveredResizeBar.PosY;

                            double curHeight;
                            double curPosY;

                            do
                            {
                                curHeight = newHeight;
                                curPosY = newPosY;

                                foreach (var rb in VerticalResizeBars)
                                {
                                    if (Math.Abs(rb.PosX - HoveredResizeBar.PosX) < DoubleTolerance)
                                    {
                                        if (Math.Abs(rb.PosY - (curPosY + curHeight)) < DoubleTolerance)
                                        {
                                            newHeight += rb.Height;
                                        }
                                        else if (Math.Abs(rb.PosY + rb.Height- curPosY) < DoubleTolerance)
                                        {
                                            newHeight += rb.Height;
                                            newPosY = rb.PosY;
                                        }
                                    }
                                }
                            } while (Math.Abs(curHeight - newHeight) > DoubleTolerance || Math.Abs(curPosY - newPosY) > DoubleTolerance);

                            HoveredResizeBar.Height = newHeight;
                            HoveredResizeBar.PosY = newPosY;

                            ResizeMinPos = 0;
                            ResizeMaxPos = PanelWidth;

                            foreach (var curViewModel in ViewModels)
                            {
                                if ((curViewModel.PosY > HoveredResizeBar.PosY && curViewModel.PosY < HoveredResizeBar.PosY + HoveredResizeBar.Height) ||
                                    (curViewModel.PosY + curViewModel.Height > HoveredResizeBar.PosY && curViewModel.PosY + curViewModel.Height < HoveredResizeBar.PosY + HoveredResizeBar.Height) ||
                                    (curViewModel.PosY <= HoveredResizeBar.PosY && curViewModel.PosY + curViewModel.Height >= HoveredResizeBar.PosY + HoveredResizeBar.Height))
                                {
                                    if (curViewModel.PosX < HoveredResizeBar.PosX)
                                        ResizeMinPos = Math.Max(ResizeMinPos, curViewModel.PosX + MinPanelSize);

                                    if (curViewModel.PosX + curViewModel.Width > HoveredResizeBar.PosX)
                                        ResizeMaxPos = Math.Min(ResizeMaxPos, curViewModel.PosX + curViewModel.Width - MinPanelSize);
                                }
                            }

                            ResizeMinPos = Math.Min(ResizeMinPos, HoveredResizeBar.PosX);
                            ResizeMaxPos = Math.Max(ResizeMaxPos, HoveredResizeBar.PosX);
                        }
                    }
                    else
                    {
                        foreach (var curViewModel in ViewModels)
                        {
                            if (PanelMouseX >= curViewModel.PosX && PanelMouseY >= curViewModel.PosY &&
                                PanelMouseX < curViewModel.PosX + curViewModel.Width &&
                                PanelMouseY < curViewModel.PosY + curViewModel.Height)
                            {
                                CurrentlyDraggedViewModel = curViewModel;
                                break;
                            }
                        }
                    }
                }
                else if (arg.ClickCount == 2)
                {
                    DoubleClicked = true;
                    foreach (var curViewModel in ViewModels)
                    {
                        if (PanelMouseX >= curViewModel.PosX && PanelMouseY >= curViewModel.PosY &&
                            PanelMouseX < curViewModel.PosX + curViewModel.Width &&
                            PanelMouseY < curViewModel.PosY + curViewModel.Height)
                        {
                            OpenEditWindow(curViewModel);
                            break;
                        }
                    }
                }
            }
        }

        private ICommand mouseLeftButtonUpCommand;

        public ICommand MouseLeftButtonUp
        {
            get
            {
                if (mouseLeftButtonUpCommand == null)
                {
                    mouseLeftButtonUpCommand = new RelayCommand(MouseLeftButtonUpExecute, param => true);
                }
                return mouseLeftButtonUpCommand;
            }
        }
        
        /// <summary>
        /// Handeles the finished resizing and the finished dragging of viewModels.
        /// </summary>
        /// <param name="param"></param>
        private void MouseLeftButtonUpExecute(object param)
        {
            if (param is MouseButtonEventArgs arg)
            {
                if (arg.ClickCount == 1)
                {
                    if (CurrentlyResizing)
                    {
                        if (HorizontalResizeBars.Contains(HoveredResizeBar))
                        {
                            CurrentlyResizing = false;

                            foreach (var curViewModel in ViewModels)
                            {
                                if ((curViewModel.PosX > HoveredResizeBar.PosX &&
                                     curViewModel.PosX < HoveredResizeBar.PosX + HoveredResizeBar.Width) ||
                                    (curViewModel.PosX + curViewModel.Width > HoveredResizeBar.PosX &&
                                     curViewModel.PosX + curViewModel.Width <
                                     HoveredResizeBar.PosX + HoveredResizeBar.Width) ||
                                    (curViewModel.PosX <= HoveredResizeBar.PosX &&
                                     curViewModel.PosX + curViewModel.Width >=
                                     HoveredResizeBar.PosX + HoveredResizeBar.Width))
                                {
                                    if (Math.Abs(curViewModel.PosY - ResizeStartPos) < 0.001)
                                    {
                                        curViewModel.Height = curViewModel.Height + curViewModel.PosY - HoveredResizeBar.PosY;
                                        curViewModel.PosY = HoveredResizeBar.PosY;
                                    }
                                    if (Math.Abs(curViewModel.PosY + curViewModel.Height - ResizeStartPos) < 0.001)
                                    {
                                        curViewModel.Height = HoveredResizeBar.PosY - curViewModel.PosY;
                                    }
                                }
                            }

                            UpdateResizeBars();
                        }
                        else if (VerticalResizeBars.Contains(HoveredResizeBar))
                        {
                            CurrentlyResizing = false;

                            foreach (var curViewModel in ViewModels)
                            {
                                if ((curViewModel.PosY > HoveredResizeBar.PosY && curViewModel.PosY < HoveredResizeBar.PosY + HoveredResizeBar.Height) ||
                                    (curViewModel.PosY + curViewModel.Height > HoveredResizeBar.PosY && curViewModel.PosY + curViewModel.Height < HoveredResizeBar.PosY + HoveredResizeBar.Height) ||
                                    (curViewModel.PosY <= HoveredResizeBar.PosY && curViewModel.PosY + curViewModel.Height >= HoveredResizeBar.PosY + HoveredResizeBar.Height))
                                {
                                    if (Math.Abs(curViewModel.PosX - ResizeStartPos) < 0.001)
                                    {
                                        curViewModel.Width = curViewModel.Width + curViewModel.PosX - HoveredResizeBar.PosX;
                                        curViewModel.PosX = HoveredResizeBar.PosX;
                                    }
                                    if (Math.Abs(curViewModel.PosX + curViewModel.Width - ResizeStartPos) < 0.001)
                                    {
                                        curViewModel.Width = HoveredResizeBar.PosX - curViewModel.PosX;
                                    }
                                }
                            }

                            UpdateResizeBars();
                        }
                    }
                    else if (CurrentlyDraggedViewModel != null)
                    {
                        foreach (var curViewModel in ViewModels)
                        {
                            if (PanelMouseX >= curViewModel.PosX && PanelMouseY >= curViewModel.PosY &&
                                PanelMouseX < curViewModel.PosX + curViewModel.Width &&
                                PanelMouseY < curViewModel.PosY + curViewModel.Height)
                            {
                                double distanceTop = PanelMouseY - curViewModel.PosY;
                                double distanceLeft = PanelMouseX - curViewModel.PosX;
                                double distanceBottom = curViewModel.PosY + curViewModel.Height - PanelMouseY;
                                double distanceRight = curViewModel.PosX + curViewModel.Width - PanelMouseX;

                                if (distanceTop <= Math.Min(Math.Min(distanceRight, distanceBottom), distanceLeft))
                                    AddViewModel(curViewModel, EDirection.Top);
                                else if(distanceBottom <= Math.Min(Math.Min(distanceRight, distanceTop), distanceLeft))
                                    AddViewModel(curViewModel, EDirection.Bottom);
                                else if(distanceLeft <= Math.Min(Math.Min(distanceRight, distanceBottom), distanceTop))
                                    AddViewModel(curViewModel, EDirection.Left);
                                else
                                    AddViewModel(curViewModel, EDirection.Right);
                                break;
                            }
                        }

                        CurrentlyDraggedViewModel = null;
                    }
                }
            }
        }
        
        private ICommand mouseCaptureCommand;

        public ICommand MouseCapture
        {
            get
            {
                if (mouseCaptureCommand == null)
                {
                    mouseCaptureCommand = new RelayCommand(MouseCaptureExecute, param => true);
                }
                return mouseCaptureCommand;
            }
        }

        /// <summary>
        /// Capture the mouse in the current UIElement.
        /// </summary>
        /// <param name="param"></param>
        private void MouseCaptureExecute(object param)
        {
            if (!DoubleClicked)
            {
                (param as UIElement)?.CaptureMouse();
            }
            else
            {
                DoubleClicked = false;
            }
        }

        private ICommand mouseReleaseCaptureCommand;

        public ICommand MouseReleaseCapture
        {
            get
            {
                if (mouseReleaseCaptureCommand == null)
                {
                    mouseReleaseCaptureCommand = new RelayCommand(MouseReleaseCaptureExecute, param => true);
                }
                return mouseReleaseCaptureCommand;
            }
        }

        /// <summary>
        /// Release the MouseCapture in the current UIElement.
        /// </summary>
        /// <param name="param"></param>
        private void MouseReleaseCaptureExecute(object param)
        {
            (param as UIElement)?.ReleaseMouseCapture();
        }

        private ICommand saveLayoutCommand;

        public ICommand SaveLayout
        {
            get
            {
                if (saveLayoutCommand == null)
                {
                    saveLayoutCommand = new RelayCommand(SaveLayoutExecute, param => ViewModels.Count != 0);
                }
                return saveLayoutCommand;
            }
        }

        /// <summary>
        /// Openes a SaveFileDialog and tries to save the current layout to the selected file.
        /// </summary>
        /// <param name="param"></param>
        private void SaveLayoutExecute(object param)
        {
            SettingsToSave settings = new SettingsToSave();
            settings.ViewModels = ViewModels;
            settings.UpdateIntervall = UpdateIntervall;
            settings.MaxValueListSize = MaxValueListSize;
            settings.ServerIp = ServerIp;
            settings.ServerPort = ServerPort;
            string json = JsonConvert.SerializeObject(settings, new JsonSerializerSettings{TypeNameHandling = TypeNameHandling.Auto});

            SaveFileDialog sfd = new SaveFileDialog();

            sfd.Filter = "json (*.json)|*.json|All files (*.*)|*.*";
            sfd.FilterIndex = 1;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() == true)
            {
                try
                {
                    File.WriteAllText(sfd.FileName, json);
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not save layout!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
        }

        private ICommand loadLayoutCommand;

        public ICommand LoadLayout
        {
            get
            {
                if (loadLayoutCommand == null)
                {
                    loadLayoutCommand = new RelayCommand(LoadLayoutExecute, param => true);
                }
                return loadLayoutCommand;
            }
        }

        /// <summary>
        /// Openes a OpenFileDialog and tries to load its content as Layout.
        /// </summary>
        /// <param name="param"></param>
        private void LoadLayoutExecute(object param)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter = "json (*.json)|*.json|All files (*.*)|*.*";
            ofd.FilterIndex = 1;
            ofd.RestoreDirectory = true;

            if (ofd.ShowDialog() == true)
            {
                try
                {
                    string json = File.ReadAllText(ofd.FileName);
                    var settings = JsonConvert.DeserializeObject<SettingsToSave>(json, new JsonSerializerSettings{TypeNameHandling = TypeNameHandling.Auto});
                    ViewModels = settings.ViewModels;
                    UpdateIntervall = settings.UpdateIntervall;
                    MaxValueListSize = settings.MaxValueListSize;
                    ServerIp = settings.ServerIp;
                    ServerPort = settings.ServerPort;
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not load layout!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                foreach (var viewModel in ViewModels)
                {
                    viewModel.Refcycle = Refcycle;
                }
                
                double oldWidth = 0;
                double oldHeight = 0;

                foreach (var curViewModel in ViewModels)
                {
                    oldWidth = Math.Max(oldWidth, curViewModel.PosX + curViewModel.Width);
                    oldHeight = Math.Max(oldHeight, curViewModel.PosY + curViewModel.Height);
                }

                var scaleX = 1.0 * PanelWidth / oldWidth;
                var scaleY = 1.0 * PanelHeight / oldHeight;

                foreach (var curViewModel in ViewModels)
                {
                    double newW = ((curViewModel.PosX + curViewModel.Width) * scaleX);
                    double newH = ((curViewModel.PosY + curViewModel.Height) * scaleY);
                    curViewModel.PosX = (curViewModel.PosX * scaleX);
                    curViewModel.PosY = (curViewModel.PosY * scaleY);
                    curViewModel.Width = newW - curViewModel.PosX;
                    curViewModel.Height = newH - curViewModel.PosY;
                }

                UpdateResizeBars();
            }
        }
        
        private ICommand openSettingsCommand;

        public ICommand OpenSettings
        {
            get
            {
                if (openSettingsCommand == null)
                {
                    openSettingsCommand = new RelayCommand(OpenSettingsExecute, param => true);
                }
                return openSettingsCommand;
            }
        }

        /// <summary>
        /// Openes the SettingsView.
        /// </summary>
        /// <param name="param"></param>
        private void OpenSettingsExecute(object param)
        {
            SettingsView sv = new SettingsView();
            sv.DataContext = this;
            sv.ShowDialog();
        }
        #endregion

        #region Helperfunctions
        /// <summary>
        /// Openes the PanelEditorView for the specific viewModel.
        /// </summary>
        /// <param name="curViewModel"></param>
        private void OpenEditWindow(BasePanelViewModel curViewModel)
        {
            var editView = new PanelEditorView();
            var editViewModel = new PanelEditorViewModel(curViewModel);
            editView.DataContext = editViewModel;
            editView.ShowDialog();
            if (editViewModel.Saved)
            {
                for (int i = 0; i < ViewModels.Count; ++i)
                {
                    if (ViewModels[i] == curViewModel)
                    {
                        ViewModels[i] = editViewModel.BasePanelVm;
                    }
                }
            }
        }

        /// <summary>
        /// Updates the HorizontalResizeBars and VerticalResizeBars.
        /// </summary>
        void UpdateResizeBars()
        {
            HorizontalResizeBars.Clear();
            VerticalResizeBars.Clear();

            if (ViewModels.Count <= 1)
                return;

            foreach (var curViewModel in ViewModels)
            {
                if (Math.Abs(curViewModel.PosY) > DoubleTolerance)
                {
                    HorizontalResizeBars.Add(new ResizeBar {Width = curViewModel.Width, PosX = curViewModel.PosX, PosY = curViewModel.PosY });
                }
                if (Math.Abs(curViewModel.PosX) > DoubleTolerance)
                {
                    VerticalResizeBars.Add(new ResizeBar { Height = curViewModel.Height, PosX = curViewModel.PosX, PosY = curViewModel.PosY });
                }
            }
        }

        /// <summary>
        /// Removes a viewModel and tries to find a viewModel that shares a side with it. If such a viewModel is found, it resizes it.
        /// Otherwise it calls RemoveViewModelWithMultiple with every possible direction. Afterwards it removes the viewModel.
        /// </summary>
        /// <param name="viewModel"></param>
        void RemoveViewModel(BasePanelViewModel viewModel)
        {
            bool filled = false;

            foreach (var curViewModel in ViewModels)
            {
                if (curViewModel != viewModel)
                {
                    if (Math.Abs(curViewModel.PosX - viewModel.PosX) < DoubleTolerance && Math.Abs(curViewModel.Width - viewModel.Width) < DoubleTolerance)
                    {
                        if (Math.Abs(curViewModel.PosY + curViewModel.Height - viewModel.PosY) < DoubleTolerance)
                        {
                            curViewModel.Height += viewModel.Height;
                            filled = true;
                            break;
                        }

                        if (Math.Abs(viewModel.PosY + viewModel.Height - curViewModel.PosY) < DoubleTolerance)
                        {
                            curViewModel.PosY = viewModel.PosY;
                            curViewModel.Height += viewModel.Height;
                            filled = true;
                            break;
                        }
                    }
                    else if (Math.Abs(curViewModel.PosY - viewModel.PosY) < DoubleTolerance && Math.Abs(curViewModel.Height - viewModel.Height) < DoubleTolerance)
                    {
                        if (Math.Abs(curViewModel.PosX + curViewModel.Width - viewModel.PosX) < DoubleTolerance)
                        {
                            curViewModel.Width += viewModel.Width;
                            filled = true;
                            break;
                        }

                        if (Math.Abs(viewModel.PosX + viewModel.Width - curViewModel.PosX) < DoubleTolerance)
                        {
                            curViewModel.PosX = viewModel.PosX;
                            curViewModel.Width += viewModel.Width;
                            filled = true;
                            break;
                        }
                    }
                }
            }

            if (filled == false)
            {
                foreach (EDirection dir in Enum.GetValues(typeof(EDirection)))
                {
                    if (RemoveViewModelWithMultiple(viewModel, dir))
                    {
                        break;
                    }
                }
            }

            ViewModels.Remove(viewModel);
            UpdateResizeBars();
        }

        /// <summary>
        /// Tries to remove a viewmodel and resizes the Panels from a specific direction.
        /// Therefore it checks if it can find a number of viewModels that share exactly the side of the viewModel.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="direction"></param>
        /// <returns>True if </returns>
        bool RemoveViewModelWithMultiple(BasePanelViewModel viewModel, EDirection direction)
        {
            List<BasePanelViewModel> viewModelsInside = new List<BasePanelViewModel>();
            BasePanelViewModel viewModelMin = null;
            BasePanelViewModel viewModelMax = null;

            if (direction == EDirection.Top)
            {
                foreach (var curViewModel in ViewModels)
                {
                    if (curViewModel != viewModel)
                    {
                        if (Math.Abs(curViewModel.PosY + curViewModel.Height - viewModel.PosY) < DoubleTolerance)
                        {
                            if (Math.Abs(curViewModel.PosX - viewModel.PosX) < DoubleTolerance &&
                                curViewModel.Width < viewModel.Width)
                            {
                                viewModelMin = curViewModel;
                            }
                            else if (Math.Abs(curViewModel.PosX + curViewModel.Width -
                                              (viewModel.PosX + viewModel.Width)) < DoubleTolerance &&
                                     curViewModel.Width < viewModel.Width)
                            {
                                viewModelMax = curViewModel;
                            }
                            else if (curViewModel.PosX > viewModel.PosX && curViewModel.PosX + curViewModel.Width <
                                     viewModel.PosX + viewModel.Width)
                            {
                                viewModelsInside.Add(curViewModel);
                            }
                        }
                    }
                }
            }
            else if (direction == EDirection.Bottom)
            {
                foreach (var curViewModel in ViewModels)
                {
                    if (curViewModel != viewModel)
                    {
                        if (Math.Abs(curViewModel.PosY - (viewModel.PosY + viewModel.Height)) < DoubleTolerance)
                        {
                            if (Math.Abs(curViewModel.PosX - viewModel.PosX) < DoubleTolerance &&
                                curViewModel.Width < viewModel.Width)
                            {
                                viewModelMin = curViewModel;
                            }
                            else if (Math.Abs(curViewModel.PosX + curViewModel.Width -
                                              (viewModel.PosX + viewModel.Width)) < DoubleTolerance &&
                                     curViewModel.Width < viewModel.Width)
                            {
                                viewModelMax = curViewModel;
                            }
                            else if (curViewModel.PosX > viewModel.PosX && curViewModel.PosX + curViewModel.Width <
                                     viewModel.PosX + viewModel.Width)
                            {
                                viewModelsInside.Add(curViewModel);
                            }
                        }
                    }
                }
            }
            else if (direction == EDirection.Left)
            {
                foreach (var curViewModel in ViewModels)
                {
                    if (curViewModel != viewModel)
                    {
                        if (Math.Abs(curViewModel.PosX + curViewModel.Width - viewModel.PosX) < DoubleTolerance)
                        {
                            if (Math.Abs(curViewModel.PosY - viewModel.PosY) < DoubleTolerance &&
                                curViewModel.Height < viewModel.Height)
                            {
                                viewModelMin = curViewModel;
                            }
                            else if (Math.Abs(curViewModel.PosY + curViewModel.Height -
                                              (viewModel.PosY + viewModel.Height)) < DoubleTolerance &&
                                     curViewModel.Height < viewModel.Height)
                            {
                                viewModelMax = curViewModel;
                            }
                            else if (curViewModel.PosY > viewModel.PosY && curViewModel.PosY + curViewModel.Height <
                                     viewModel.PosY + viewModel.Height)
                            {
                                viewModelsInside.Add(curViewModel);
                            }
                        }
                    }
                }
            }
            else if (direction == EDirection.Right)
            {
                foreach (var curViewModel in ViewModels)
                {
                    if (curViewModel != viewModel)
                    {
                        if (Math.Abs(curViewModel.PosX - (viewModel.PosX + viewModel.Width)) < DoubleTolerance)
                        {
                            if (Math.Abs(curViewModel.PosY - viewModel.PosY) < DoubleTolerance && curViewModel.Height < viewModel.Height)
                            {
                                viewModelMin = curViewModel;
                            }
                            else if (Math.Abs(curViewModel.PosY + curViewModel.Height - (viewModel.PosY + viewModel.Height)) < DoubleTolerance && curViewModel.Height < viewModel.Height)
                            {
                                viewModelMax = curViewModel;
                            }
                            else if (curViewModel.PosY > viewModel.PosY && curViewModel.PosY + curViewModel.Height < viewModel.PosY + viewModel.Height)
                            {
                                viewModelsInside.Add(curViewModel);
                            }
                        }
                    }
                }
            }

            if (viewModelMin == null || viewModelMax == null)
                return false;

            if (direction == EDirection.Top)
            {
                viewModelMin.Height += viewModel.Height;
                viewModelMax.Height += viewModel.Height;
                foreach (var curViewModel in viewModelsInside)
                {
                    curViewModel.Height += viewModel.Height;
                }
            }
            else if (direction == EDirection.Bottom)
            {
                viewModelMin.Height += viewModel.Height;
                viewModelMin.PosY = viewModel.PosY;
                viewModelMax.Height += viewModel.Height;
                viewModelMax.PosY = viewModel.PosY;
                foreach (var curViewModel in viewModelsInside)
                {
                    curViewModel.Height += viewModel.Height;
                    curViewModel.PosY = viewModel.PosY;
                }
            }
            else if (direction == EDirection.Left)
            {
                viewModelMin.Width += viewModel.Width;
                viewModelMax.Width += viewModel.Width;
                foreach (var curViewModel in viewModelsInside)
                {
                    curViewModel.Width += viewModel.Width;
                }
            }
            else if (direction == EDirection.Right)
            {
                viewModelMin.Width += viewModel.Width;
                viewModelMin.PosX = viewModel.PosX;
                viewModelMax.Width += viewModel.Width;
                viewModelMax.PosX = viewModel.PosX;
                foreach (var curViewModel in viewModelsInside)
                {
                    curViewModel.Width += viewModel.Width;
                    curViewModel.PosX = viewModel.PosX;
                }
            }

            return true;
        }

        /// <summary>
        /// Adds a viewModel to the ViewModels at a specific direction. Whether it is inserted
        /// depends on whether a ViewModel is being dragged or right clicked.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="direction"></param>
        void AddViewModel(BasePanelViewModel viewModel, EDirection direction = DefaultAddDirection)
        {
            if (CurrentlyDraggedViewModel == null)
            {
                if (ViewModels.Count == 0)
                {
                    viewModel.PosX = 0;
                    viewModel.PosY = 0;
                    viewModel.Width = PanelWidth;
                    viewModel.Height = PanelHeight;

                    ViewModels.Add(viewModel);

                    return;
                }

                var curViewModel = CurrentlyRightClickedViewModel;

                if (curViewModel == null)
                {
                    curViewModel = ViewModels[0];
                }

                InsertViewModel(curViewModel, viewModel, direction);
            }
            else if(CurrentlyDraggedViewModel != viewModel)
            {
                RemoveViewModel(CurrentlyDraggedViewModel);
                InsertViewModel(viewModel, CurrentlyDraggedViewModel, direction);
                CurrentlyDraggedViewModel = null;
            }
        }

        /// <summary>
        /// Insert a viewmodel next to another viewmodel at a specific direction.
        /// </summary>
        /// <param name="existingViewModel"></param>
        /// <param name="newViewModel"></param>
        /// <param name="direction"></param>
        void InsertViewModel(BasePanelViewModel existingViewModel, BasePanelViewModel newViewModel, EDirection direction = DefaultAddDirection)
        {
            if (direction == EDirection.Bottom)
            {
                newViewModel.PosX = existingViewModel.PosX;
                newViewModel.PosY = existingViewModel.PosY + existingViewModel.Height / 2;
                newViewModel.Width = existingViewModel.Width;
                newViewModel.Height = existingViewModel.Height / 2;
                existingViewModel.Height = existingViewModel.Height / 2;
            }
            else if (direction == EDirection.Right)
            {
                newViewModel.PosX = existingViewModel.PosX + existingViewModel.Width / 2;
                newViewModel.PosY = existingViewModel.PosY;
                newViewModel.Width = existingViewModel.Width / 2;
                newViewModel.Height = existingViewModel.Height;
                existingViewModel.Width = existingViewModel.Width / 2;
            }
            else if (direction == EDirection.Top)
            {
                newViewModel.PosX = existingViewModel.PosX;
                newViewModel.PosY = existingViewModel.PosY;
                existingViewModel.PosY = existingViewModel.PosY + existingViewModel.Height / 2;
                newViewModel.Width = existingViewModel.Width;
                newViewModel.Height = existingViewModel.Height / 2;
                existingViewModel.Height = existingViewModel.Height / 2;
            }
            else if (direction == EDirection.Left)
            {
                newViewModel.PosX = existingViewModel.PosX;
                existingViewModel.PosX = existingViewModel.PosX + existingViewModel.Width / 2;
                newViewModel.PosY = existingViewModel.PosY;
                newViewModel.Width = existingViewModel.Width / 2;
                newViewModel.Height = existingViewModel.Height;
                existingViewModel.Width = existingViewModel.Width / 2;
            }

            ViewModels.Add(newViewModel);

            UpdateResizeBars();
        }
        #endregion

        public MainViewModel()
        {

            // create sensors
            Refcycle.CreateNewSensor(10, "Flow Temperature");
            Refcycle.CreateNewSensor(11, "Rewind Temperature");
            Refcycle.CreateNewSensor(12, "EQ Temperature (In)");
            Refcycle.CreateNewSensor(13, "EQ Temperature (Out)");
            Refcycle.CreateNewSensor(14, "Hot gas Temperature");
            Refcycle.CreateNewSensor(16, "Hypothermia Temperature");
            Refcycle.CreateNewSensor(17, "Compressor back Temperature");
            Refcycle.CreateNewSensor(18, "Condesation Pressure");
            Refcycle.CreateNewSensor(19, "Evapuation Pressure");
            Refcycle.CreateNewSensor(20, "Sole Pressure");
            Refcycle.CreateNewSensor(21, "Expansion Ventile (heat)");
            Refcycle.CreateNewSensor(22, "Expansion Ventile (cool)");
            Refcycle.CreateNewSensor(23, "Compressor");
            Refcycle.CreateNewSensor(24, "4-way Valve");
            Refcycle.CreateNewSensor(42, "Condensation Temperature");

            worker = new BackgroundWorker();
            worker.DoWork += WorkerDoWork;
            StartMeasurement();

            DoMeasurement = new StartMeasureCommand(this);

            UpdateIntervall = 5;
            MaxValueListSize = 500;
          
        }

        public void StartMeasurement()
        {
            worker.RunWorkerAsync();
        }

        public ICommand DoMeasurement { get; set; }

        private void WorkerDoWork(object sender, DoWorkEventArgs e)
        {

            while (true)
            {

                JsonResponse resp = ReadSensorValues();

                if (resp.Result)
                {
                    Connected = true;
                    
                    Application.Current.Dispatcher.Invoke(new Action(delegate ()
                    {
                        foreach (var val in resp.Values)
                        {

                            Refcycle.AddNewValue(val.Id, val.Value.ToString());
                        }

                    }
                    ));

                }
                else 
                {
                    Connected = false;
                }


                Thread.Sleep(UpdateIntervall*1000);
            }
        }




        public JsonResponse ReadSensorValues()
        {


            string serverAddress = "http://" + ServerIp + ":" + ServerPort + "/api/module/datapoint";
           
            try
            {
                var client = new RestClient(serverAddress);
                var restRequest = new RestRequest("/values", Method.POST);


                // every sensor in the dictionary is queried
                JArray array = new JArray();
                foreach (var s in Refcycle.Sensors)
                {
                    array.Add(s.Key);
                }

                JObject objectbody = new JObject();
                objectbody.Add("module", 1);
                objectbody.Add("id", array);

                restRequest.AddParameter("application/json", objectbody, ParameterType.RequestBody);

                IRestResponse restResponse = client.Execute(restRequest);

                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    JsonResponse result = JsonConvert.DeserializeObject<JsonResponse>(restResponse.Content);
                    return result;

                }

                return new JsonResponse();

            }
            catch (Exception)
            {
                return new JsonResponse();
            }
    
        }
    }
}
