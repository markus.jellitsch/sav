﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using HeliothermRefCycle.Models;
using Newtonsoft.Json;
using PropertyChanged;

namespace HeliothermRefCycle.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public abstract class BasePanelViewModel
    {
        public abstract BasePanelViewModel Copy();

        [JsonIgnore]
        public virtual RefrigerationCycle Refcycle { get; set; }

        public virtual double Width { get; set; }
        public virtual double Height { get; set; }

        public virtual double PosX { get; set; }
        public virtual double PosY { get; set; }
    }
}
