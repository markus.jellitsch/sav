﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using Prism.Mvvm;
using PropertyChanged;

namespace HeliothermRefCycle.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class PanelEditorViewModel
    {
        private static readonly Dictionary<string, System.Type> ViewModelNames = new Dictionary<string, System.Type>()
        {
            {"Cooling circuit", typeof(CoolingCircuitPanelViewModel)},
            {"Graph", typeof(GraphPanelViewModel)}
        };

        private string selectedViewModel;

        private BasePanelViewModel originalBasePanelVm;
        public BasePanelViewModel BasePanelVm { get; set; }

        public bool Saved { get; set; } = false;

        public double PanelWidth { get; set; }
        public double PanelHeight { get; set; }

        public List<string> AvailableViewModels { get => ViewModelNames.Keys.ToList(); }

        public string SelectedViewModel
        {
            get => selectedViewModel;
            set
            {
                selectedViewModel = value;

                if (ViewModelNames[selectedViewModel] == typeof(CoolingCircuitPanelViewModel))
                {
                    BasePanelVm = new CoolingCircuitPanelViewModel(BasePanelVm.Refcycle);
                }
                else if (ViewModelNames[selectedViewModel] == typeof(GraphPanelViewModel))
                {
                    BasePanelVm = new GraphPanelViewModel(BasePanelVm.Refcycle);
                }
                else
                {
                    throw new NotImplementedException();
                }

                BasePanelVm.Width = PanelWidth;
                BasePanelVm.Height = PanelHeight;
            }
        }

        public int WindowWidth { get; set; } = 800;

        private ICommand gridResizeCommand;
        public ICommand GridResized
        {
            get
            {
                if (gridResizeCommand == null)
                {
                    gridResizeCommand = new RelayCommand(GridResizedExecute, param => true);
                }
                return gridResizeCommand;
            }
        }

        /// <summary>
        /// Update the viewModel's size.
        /// </summary>
        /// <param name="param"></param>
        private void GridResizedExecute(object param)
        {
            var element = param as FrameworkElement;
            if (element == null)
                return;
            
            PanelWidth = element.ActualWidth;
            PanelHeight = element.ActualHeight;

            BasePanelVm.Width = PanelWidth;
            BasePanelVm.Height = PanelHeight;
        }
        
        private ICommand windowLoadCommand;
        public ICommand WindowLoad
        {
            get
            {
                if (windowLoadCommand == null)
                {
                    windowLoadCommand = new RelayCommand(WindowLoadExecute, param => true);
                }
                return windowLoadCommand;
            }
        }

        /// <summary>
        /// Set the viewModel's size.
        /// </summary>
        /// <param name="param"></param>
        private void WindowLoadExecute(object param)
        {
            var element = param as FrameworkElement;
            if (element == null)
                return;
            
            PanelWidth = element.ActualWidth;
            PanelHeight = element.ActualHeight;

            BasePanelVm.Width = PanelWidth;
            BasePanelVm.Height = PanelHeight;
        }
        
        private ICommand savePressedCommand;

        public ICommand SavePressed
        {
            get
            {
                if (savePressedCommand == null)
                {
                    savePressedCommand = new RelayCommand(SavePressedExecute, param => true);
                }
                return savePressedCommand;
            }
        }

        /// <summary>
        /// Adjust the viewModel's position and size and close the Window.
        /// </summary>
        /// <param name="param"></param>
        private void SavePressedExecute(object param)
        {
            BasePanelVm.PosX = originalBasePanelVm.PosX;
            BasePanelVm.PosY = originalBasePanelVm.PosY;
            BasePanelVm.Width = originalBasePanelVm.Width;
            BasePanelVm.Height = originalBasePanelVm.Height;
            Saved = true;

            (param as Window)?.Close();
        }

        private ICommand cancelPressedCommand;

        public ICommand CancelPressed
        {
            get
            {
                if (cancelPressedCommand == null)
                {
                    cancelPressedCommand = new RelayCommand(CancelExecute, param => true);
                }
                return cancelPressedCommand;
            }
        }

        /// <summary>
        /// Close the window.
        /// </summary>
        /// <param name="param"></param>
        private void CancelExecute(object param)
        {
            (param as Window)?.Close();
        }

        public PanelEditorViewModel(BasePanelViewModel viewModel)
        {
            BasePanelVm = viewModel.Copy();
            BasePanelVm.PosX = 0;
            BasePanelVm.PosY = 0;
            originalBasePanelVm = viewModel;

            selectedViewModel = ViewModelNames.FirstOrDefault(x => x.Value.IsInstanceOfType(viewModel)).Key;
        }
    }
}
