﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HeliothermRefCycle.Models;
using HeliothermRefCycle.Views;
using Newtonsoft.Json;
using PropertyChanged;

namespace HeliothermRefCycle.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class SensorWithName
    {
        private string name;
        public event EventHandler NameChanged;

        public string Name
        {
            get => name;
            set
            {
                name = value;
                OnNameChanged();
            }
        }

        public string DisplayName { get; set; }
        
        [JsonIgnore]
        public Sensor SensorProp { get; set; }

        protected virtual void OnNameChanged()
        {
            NameChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    [AddINotifyPropertyChangedInterface]
    public sealed class CoolingCircuitPanelViewModel : BasePanelViewModel
    {
        private readonly static int baseSizeX = 800;
        private readonly static int baseSizeY = 450;
        private double width = baseSizeX;
        private double height = baseSizeY;
        private RefrigerationCycle refcycle;

        public double ScaleX { get; set; } = 1;
        public double ScaleY { get; set; } = 1;

        /// <summary>
        /// Create a copy of the ViewModel.
        /// </summary>
        /// <returns></returns>
        public override BasePanelViewModel Copy()
        {
            var viewModel = this.MemberwiseClone() as CoolingCircuitPanelViewModel;

            viewModel.Sensors = new ObservableCollection<SensorWithName>();
            foreach (var sensor in Sensors)
            {
                viewModel.Sensors.Add(new SensorWithName(){DisplayName = sensor.DisplayName, Name = sensor.Name, SensorProp = sensor.SensorProp});
            }
            foreach (var sensor in viewModel.Sensors)
            {
                sensor.NameChanged += viewModel.UpdateSensors;
            }

            return viewModel;
        }

        [JsonIgnore]
        public override RefrigerationCycle Refcycle
        {
            get => refcycle;
            set
            {
                refcycle = value;
                UpdateSensors(this, EventArgs.Empty);
            }
        }

        public override double Width
        {
            get => width;
            set
            {
                width = value;
                UpdateSize();
            }
        }

        public override double Height
        {
            get => height;
            set
            {
                height = value;
                UpdateSize();
            }
        }

        public override double PosX { get; set; } = 0;
        public override double PosY { get; set; } = 0;
        
        public ObservableCollection<SensorWithName> Sensors { get; set; } = new ObservableCollection<SensorWithName>()
        {
            {new SensorWithName(){Name="Rewind Temperature", DisplayName = "Sensor 1:", SensorProp = null}},
            {new SensorWithName(){Name="Expansion Ventile (heat)", DisplayName = "Sensor 2:", SensorProp = null}},
            {new SensorWithName(){Name="EQ Temperature (In)", DisplayName = "Sensor 3:", SensorProp = null}},
            {new SensorWithName(){Name="EQ Temperature (Out)", DisplayName = "Sensor 4:", SensorProp = null}},
            {new SensorWithName(){Name="Compressor", DisplayName = "Sensor 5:", SensorProp = null}},
            {new SensorWithName(){Name="Condensation Temperature", DisplayName = "Sensor 6:", SensorProp = null}},
            {new SensorWithName(){Name="Flow Temperature", DisplayName = "Sensor 7:", SensorProp = null}},
            {new SensorWithName(){Name="Evapuation Pressure", DisplayName = "Sensor 8:", SensorProp = null}},
            {new SensorWithName(){Name="Condesation Pressure", DisplayName = "Sensor 9:", SensorProp = null}},
            {new SensorWithName(){Name="", DisplayName = "Sensor 10:", SensorProp = null}},
        };

        /// <summary>
        /// Update the scaling of the ViewModel.
        /// </summary>
        public void UpdateSize()
        {
            ScaleX = 1.0 * width / baseSizeX;
            ScaleY = 1.0 * height / baseSizeY;
        }

        [JsonConstructor]
        private CoolingCircuitPanelViewModel()
        {
            Sensors.Clear();
        }

        public CoolingCircuitPanelViewModel(RefrigerationCycle rc)
        {
            Refcycle = rc;

            var view = new CoolingCircuitPanelView {DataContext = this};

            foreach (var sensor in Sensors)
            {
                sensor.NameChanged += UpdateSensors;
            }
        }

        /// <summary>
        /// Check all sensor names and update the Sensor.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateSensors(object sender, EventArgs e)
        {
            foreach (var sensor in Sensors)
            {
                int key;

                if (Int32.TryParse(sensor.Name, out key) && Refcycle.Sensors.ContainsKey(key))
                {
                    sensor.SensorProp = Refcycle.Sensors[key];
                }
                else
                {
                    sensor.SensorProp = Refcycle.Sensors.FirstOrDefault(s => s.Value.Name == sensor.Name).Value;
                }
            }
        }
    }
}
